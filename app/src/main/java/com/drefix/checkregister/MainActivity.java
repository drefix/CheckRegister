package com.drefix.checkregister;

import android.os.Bundle;
import android.support.constraint.solver.ArrayLinkedVariables;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.TintableBackgroundView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Context;
import android.content.DialogInterface;
import android.view.ViewDebug;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.graphics.drawable.ColorDrawable;
import android.content.res.ColorStateList;
import android.text.Editable;
import android.text.Selection;

import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.EditText;
import android.widget.Button;
import android.app.Dialog;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.view.inputmethod.InputMethodManager;
import android.text.TextWatcher;
import android.widget.LinearLayout;
import android.widget.EditText;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.StreamCorruptedException;


import java.util.TimeZone;

public class MainActivity extends AppCompatActivity {

    private ListView mainList;
    private CardAdapter adapter;

    ArrayList<Entry> entries = new ArrayList<Entry>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        int color = getColor(getApplicationContext());
        colorPopup(color);

        entries = openEntries(MainActivity.this);

        mainList = (ListView)findViewById(R.id.mainList);
        mainList.setAdapter(new CardAdapter(this, entries));
        adapter = (CardAdapter) mainList.getAdapter();

        mainList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("What would you like to do?");

                String[] types = {"Edit", "Delete", "Cancel"};
                builder.setItems(types, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int i) {
                        switch (i){
                            case 0:
                                updateEntry(position);
                                break;
                            case 1:
                                Entry removedEntry = entries.get(position);
                                double difference;

                                if(removedEntry.getCredit() != null) {
                                    difference = removedEntry.getCredit();
                                }
                                else{
                                    difference = -(removedEntry.getDebit());
                                }

                                for (int x = position + 1; x < entries.size(); x++) {
                                    entries.get(x).balance -= difference;
                                }

                                entries.remove(position);

                                adapter.notifyDataSetChanged();
                                break;
                            case 3:
                                break;
                        }
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
                return true;
            }
        });



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        //drefix use to change the color of the fab
        //fab.setBackgroundTintList(getResources().getColorStateList(R.color.white));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Choose a Transaction");

                String[] types = {"Deposit", "Payment"};
                builder.setItems(types, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int i) {
                        newEntryPopup(i, 0);
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }

        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("Choose your color");

            String[] types = {"Red", "Orange", "Yellow", "Green", "Blue", "Violet"};
            builder.setItems(types, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int i) {
                    int tempColor = -1;
                    switch (i){
                        case 0:
                            tempColor = 0;
                            break;
                        case 1:
                            tempColor = 1;
                            break;
                        case 2:
                            tempColor = 2;
                            break;
                        case 3:
                            tempColor = 3;
                            break;
                        case 4:
                            tempColor = 4;
                            break;
                        case 5:
                            tempColor = 5;
                            break;
                    }

                    colorPopup(tempColor);
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();;
        saveEntries();

    }
    public ArrayList<Entry> openEntries(Context context) {

        ObjectInputStream input = null;
        ArrayList<Entry> ReturnClass = new ArrayList<Entry>();
        File f = new File(getFilesDir(), "entries.dre");
        try {

            input = new ObjectInputStream(new FileInputStream(f));
            ReturnClass = (ArrayList<Entry>) input.readObject();
            input.close();

        } catch (StreamCorruptedException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        if (ReturnClass.isEmpty()){

        }

        return ReturnClass;
    }


    public void saveEntries() {

        File f = new File(getFilesDir(), "entries.dre");
        try {
            FileOutputStream fos = new FileOutputStream(f);
            ObjectOutputStream objectwrite = new ObjectOutputStream(fos);
            objectwrite.writeObject(entries);
            fos.close();

            if (!f.exists()) {
                f.mkdirs();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void newEntryPopup(final int entryType, int edit){

        String[] type = {"Deposit", "Payment"};
        final int [] dateArray = new int [3];

        final Dialog sub_dialog = new Dialog(MainActivity.this);
        sub_dialog.setContentView(R.layout.entry_dialog);
        sub_dialog.setTitle(type[entryType]);

        TextView typeLabel = (TextView) sub_dialog.findViewById(R.id.trans_type_label);
        switch (entryType) {
            case 0:
                typeLabel.setText("Deposit");
                break;
            case 1:
                typeLabel.setText("Payment");
                break;
        }

        final EditText date = (EditText) sub_dialog.findViewById(R.id.date_edit);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String sDate = date.getText().toString();
                int year;
                int month;
                int day;
                if (sDate.length() == 0){
                    Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
                    year = calendar.get(Calendar.YEAR);
                    month = calendar.get(Calendar.MONTH);
                    day = calendar.get(Calendar.DATE);
                }
                else {
                    String[] dateArray = sDate.split("/");
                    year = Integer.getInteger(dateArray[2]);
                    month = Integer.getInteger(dateArray[1]);
                    day = Integer.getInteger(dateArray[0]);
                }

                InputMethodManager imm =
                        (InputMethodManager) MainActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(date.getWindowToken(), 0);
                DatePickerDialog dateDialog = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        String tempDate = "";
                        dateArray[1] = day;
                        dateArray[0] = month + 1;
                        dateArray[2] = year;

                        for (int i = 0; i < 3; i++){
                            tempDate += dateArray[i];
                            if(i != 2){
                                tempDate += "/";
                            }
                        }

                        date.setText(tempDate);

                    }
                }, year,month,day);
                dateDialog.show();


            }
        });


        final EditText description = (EditText) sub_dialog.findViewById(R.id.ref_edit);

        TextView checkLabel = (TextView) sub_dialog.findViewById(R.id.check_label);
        final EditText checkEdit = (EditText) sub_dialog.findViewById(R.id.check__edit);
        if(entryType == 1){
            checkLabel.setVisibility(View.VISIBLE);
            checkEdit.setVisibility(View.VISIBLE);
        }

        final EditText amount = (EditText) sub_dialog.findViewById(R.id.trans_edit);
        /*  drefix - add mask that enters numbers and moves the decimal point
            amount.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {}
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().matches("^\\$(\\d{1,3}(\\,\\d{3})*|(\\d+))(\\.\\d{2})?$"))
                {
                    String userInput= ""+s.toString().replaceAll("[^\\d]", "");
                    StringBuilder cashAmountBuilder = new StringBuilder(userInput);

                    while (cashAmountBuilder.length() > 3 && cashAmountBuilder.charAt(0) == '0') {
                        cashAmountBuilder.deleteCharAt(0);
                    }
                    while (cashAmountBuilder.length() < 3) {
                        cashAmountBuilder.insert(0, '0');
                    }
                    cashAmountBuilder.insert(cashAmountBuilder.length()-2, '.');
                    cashAmountBuilder.insert(0, '$');

                    amount.setText(cashAmountBuilder.toString());
                    // keeps the cursor always to the right
                    Selection.setSelection(amount.getText(), cashAmountBuilder.toString().length());

                }

            }
        }); */



        Button dialogButtonOK = (Button) sub_dialog.findViewById(R.id.dialogButtonOK);
        dialogButtonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int backtrace = 30;

                String sAmount = amount.getText().toString();
                if (sAmount.length() == 0){
                    Toast.makeText(getBaseContext(),"Please Enter an Amount",Toast.LENGTH_LONG).show();
                    return;
                }
                Double dAmount = Double.parseDouble(sAmount);

                Entry lastEntry ;
                double lastBalance;
                if (entries.size() != 0) {
                    lastEntry = entries.get(entries.size() - 1);
                    lastBalance = lastEntry.balance;
                }
                else {
                    lastBalance = 0.00;
                }

                final Entry entry = new Entry();
                entry.date = dateArray;
                entry.description = description.getText().toString();
                if (entryType == 0){
                    entry.credit = dAmount;
                    entry.balance = lastBalance + dAmount;
                    entry.checkNo = -1;
                }
                else{
                    String checkNo = checkEdit.getText().toString();
                    if(checkNo.isEmpty()){
                        entry.checkNo = -1;//checkNo;
                    }
                    else{
                        //int checkNo = Integer.parseInt(checkEdit.getText().toString());
                        entry.checkNo = Integer.parseInt(checkNo);
                    }
                    entry.debit = dAmount;
                    entry.balance = lastBalance - dAmount;
                }

                if (entries.size() < 30){
                    backtrace = (entries.size());
                }

                if (entryType == 0) {

                    ArrayList<String> dupDates = new ArrayList<String>();
                    String dateList = "";

                    for (int x = 1; x < backtrace; x++) {

                        Entry tempEntry = entries.get(entries.size() - x);
                        int isEqual;

                        if (tempEntry.getCredit() != null) {
                            isEqual = Double.compare(dAmount, tempEntry.getCredit());
                        } else {
                            continue;
                        }

                        if (isEqual == 0) {
                            String dateString = "";
                            for(int z = 0; z < 3; z++){
                                if(z <= 1){
                                    dateString += tempEntry.date[z] + "/";
                                }
                                else{
                                    dateString += tempEntry.date[z];
                                }

                            }
                            if (entry.description.equals(tempEntry.description)) {
                                    dupDates.add(dateString);
                            }
                        }
                    }

                    for (int y = 0; y < dupDates.size(); y++) {
                        if(y < (dupDates.size() -1)) {
                            dateList += dupDates.get(y) + ", ";
                        }
                        else
                        {
                            dateList += dupDates.get(y);
                        }
                    }

                    if (dupDates.size() != 0) {
                        final AlertDialog.Builder duplicateDialog = new AlertDialog.Builder(
                                MainActivity.this);

                        // set title
                        duplicateDialog.setTitle("Duplicate Entry");

                        // set dialog message
                        duplicateDialog
                                .setMessage("Similar entry found on " + dateList + ", Would you like to continue?")
                                .setCancelable(false)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        entries.add(entry);
                                        adapter.notifyDataSetChanged();
                                        sub_dialog.dismiss();
                                        dialog.cancel();

                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        sub_dialog.dismiss();
                                        dialog.cancel();
                                    }
                                });

                        // create alert dialog
                        AlertDialog alertDialog = duplicateDialog.create();

                        // show it
                        alertDialog.show();


                    } else {
                        entries.add(entry);
                        adapter.notifyDataSetChanged();
                        sub_dialog.dismiss();
                    }
                }


                if (entryType == 1) {

                    ArrayList<String> dupDates = new ArrayList<String>();
                    String dateList = "";

                    for (int x = 1; x < backtrace; x++) {

                        Entry tempEntry = entries.get(entries.size() - x);
                        int isEqual;

                        if (tempEntry.getDebit() != null) {
                            isEqual = Double.compare(dAmount, tempEntry.getDebit());
                        } else {
                            continue;
                        }

                        if (isEqual == 0) {
                            String dateString = "";
                            for(int z = 0; z < 3; z++){
                                if(z <= 1){
                                    dateString += tempEntry.date[z] + "/";
                                }
                                else{
                                    dateString += tempEntry.date[z];
                                }

                            }
                            if (entry.description.equals(tempEntry.description)) {
                                dupDates.add(dateString);
                            }
                        }
                    }

                    for (int y = 0; y < dupDates.size(); y++) {
                        if(y < (dupDates.size() -1)) {
                            dateList += dupDates.get(y) + ", ";
                        }
                        else
                        {
                            dateList += dupDates.get(y);
                        }
                    }

                    if (dupDates.size() != 0) {
                        final AlertDialog.Builder duplicateDialog = new AlertDialog.Builder(
                                MainActivity.this);

                        // set title
                        duplicateDialog.setTitle("Duplicate Entry");

                        // set dialog message
                        duplicateDialog
                                .setMessage("Similar entry found on " + dateList + ", Would you like to continue?")
                                .setCancelable(false)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        entries.add(entry);
                                        adapter.notifyDataSetChanged();
                                        sub_dialog.dismiss();
                                        dialog.cancel();

                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        sub_dialog.dismiss();
                                        dialog.cancel();
                                    }
                                });

                        // create alert dialog
                        AlertDialog alertDialog = duplicateDialog.create();

                        // show it
                        alertDialog.show();


                    } else {
                        entries.add(entry);
                        adapter.notifyDataSetChanged();
                        sub_dialog.dismiss();
                    }
                }
            }


        });
        Button dialogButtonCancel = (Button) sub_dialog.findViewById(R.id.dialogButtonCancel);
        dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sub_dialog.dismiss();
            }
        });

        sub_dialog.show();



    }

    public void updateEntry(final Integer position){

        final int entryType;

        final Entry oldEntry = entries.get(position);
        if (oldEntry.getCredit() != null){
            entryType = 0;
        }
        else{
            entryType = 1;
        }

        String[] type = {"Deposit", "Payment"};

        final Dialog sub_dialog = new Dialog(MainActivity.this);
        sub_dialog.setContentView(R.layout.entry_dialog);
        sub_dialog.setTitle(type[entryType]);

        TextView typeLabel = (TextView) sub_dialog.findViewById(R.id.trans_type_label);
        switch (entryType) {
            case 0:
                typeLabel.setText("Deposit");
                break;
            case 1:
                typeLabel.setText("Payment");
                break;
        }

        // set the custom dialog components - text, image and button
        final EditText date = (EditText) sub_dialog.findViewById(R.id.date_edit);
        int[] tempDate = oldEntry.getDate();
        String dateString = tempDate[0] + "/" + tempDate[1] + "/" + tempDate[2];
        date.setText(dateString);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
                InputMethodManager imm =
                        (InputMethodManager) MainActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(date.getWindowToken(), 0);
                DatePickerDialog dateDialog = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        String dateText = Integer.toString(day) + "/" + Integer.toString(month) +"/" + Integer.toString(year) ;
                        date.setText(dateText);
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),calendar.get(Calendar.DATE));
                dateDialog.show();


            }
        });


        final EditText description = (EditText) sub_dialog.findViewById(R.id.ref_edit);
        description.setText(oldEntry.getDescription());

        TextView checkLabel = (TextView) sub_dialog.findViewById(R.id.check_label);
        final EditText checkEdit = (EditText) sub_dialog.findViewById(R.id.check__edit);

        if(entryType == 1){
            checkLabel.setVisibility(View.VISIBLE);
            checkEdit.setVisibility(View.VISIBLE);
            if(oldEntry.getCheckNo() != -1){
                checkEdit.setText(Integer.toString(oldEntry.getCheckNo()));
            }
        }

        final EditText amount = (EditText) sub_dialog.findViewById(R.id.trans_edit);
        if(entryType == 0){
            amount.setText(oldEntry.getCredit().toString());
        }
        else{
            amount.setText(oldEntry.getDebit().toString());
        }


        Button dialogButtonOK = (Button) sub_dialog.findViewById(R.id.dialogButtonOK);
        dialogButtonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String sAmount = amount.getText().toString();
                Double dAmount = Double.parseDouble(sAmount);

                Entry entry = new Entry();
                Entry lastEntry = new Entry();
                if(position != 0) {
                    lastEntry = entries.get(position - 1);
                }
                Double lastBalance = lastEntry.balance;
                Double difference;

                int[] init = {0,0,0};

                entry.date = init;

                entry.description = description.getText().toString();
                if (entryType == 0){
                    entry.credit = dAmount;
                    if(position == 0){
                        entry.balance = dAmount;
                    }
                    else {
                        entry.balance = lastBalance + dAmount;
                    }
                    entry.checkNo = -1;
                    difference = oldEntry.credit - entry.credit;
                }
                else{
                    String checkNo = checkEdit.getText().toString();
                    if(checkNo.isEmpty()){
                        entry.checkNo = -1;//checkNo;
                    }
                    else{
                        //int checkNo = Integer.parseInt(checkEdit.getText().toString());
                        entry.checkNo = Integer.parseInt(checkNo);
                    }

                    entry.debit = dAmount;
                    if (position == 0){
                        entry.balance = dAmount;
                    }
                    else {
                        entry.balance = lastBalance - dAmount;
                    }
                    difference = oldEntry.debit - entry.debit;
                }

                entries.set(position, entry);

                Entry tempEntry;

                for (int i = position + 1; i < entries.size(); i++) {
                    tempEntry = entries.get(i);
                    tempEntry.balance = tempEntry.getBalance() - difference;
                    entries.set(i, tempEntry);
                }

                adapter.notifyDataSetChanged();
                sub_dialog.dismiss();

            }


        });
        Button dialogButtonCancel = (Button) sub_dialog.findViewById(R.id.dialogButtonCancel);
        dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sub_dialog.dismiss();
            }
        });

        sub_dialog.show();



    }

    public void colorPopup(int color){


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        // color = "Red", "Orange", "Yellow", "Green", "Blue", "Violet"

        switch (color) {
            case 0:
                getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                        .getColor(R.color.dreRed)));
                fab.setBackgroundTintList(getResources().getColorStateList(R.color.dreRed));
                break;
            case 1:
                getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                        .getColor(R.color.dreOrange)));
                fab.setBackgroundTintList(getResources().getColorStateList(R.color.dreOrange));
                break;
            case 2:
                getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                        .getColor(R.color.dreYellow)));
                fab.setBackgroundTintList(getResources().getColorStateList(R.color.dreYellow));
                break;

            case 3:
                getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                        .getColor(R.color.dreGreen)));
                fab.setBackgroundTintList(getResources().getColorStateList(R.color.dreGreen));
                break;

            case 4:
                getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                        .getColor(R.color.dreBlue)));
                fab.setBackgroundTintList(getResources().getColorStateList(R.color.dreBlue));
                break;

            case 5:
                getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                        .getColor(R.color.dreViolet)));
                fab.setBackgroundTintList(getResources().getColorStateList(R.color.dreViolet));
                break;
        }

        setColor(getApplicationContext(),color);

    }

    public static void setColor(Context context, int color) {


            SharedPreferences prefs = context.getSharedPreferences("check_register", MODE_PRIVATE);
            prefs.edit().putInt("color", color).commit();

        }

    public static int getColor(Context context) {

        SharedPreferences prefs = context.getSharedPreferences("check_register", MODE_PRIVATE);
            return prefs.getInt("color", -1);
        }




}



