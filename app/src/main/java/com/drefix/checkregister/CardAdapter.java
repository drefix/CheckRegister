package com.drefix.checkregister;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by drefix on 9/21/17.
 */

public class CardAdapter extends BaseAdapter{

    ArrayList<Entry> entries;
    Context context;
    private static LayoutInflater inflater=null;
    public CardAdapter(Context context, ArrayList<Entry> entries) {
        // TODO Auto-generated constructor stub
        this.entries = entries;
        this.context=context;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return entries.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {

        TextView date;
        TextView description;
        TextView checkNo;
        TextView debit;
        TextView credit;
        TextView balance;
    }
    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {

        Holder holder=new Holder();
        String tempDate = "";
        View rowView = null;
        rowView = inflater.inflate(R.layout.cards, null);

        if(position % 2 == 0){
            rowView.setBackgroundColor(Color.LTGRAY);
        }


        holder.date=(TextView) rowView.findViewById(R.id.card_date);
        for (int i = 0; i < 3; i++){
            tempDate += entries.get(position).getDate()[i];
            if(i != 2){
                tempDate += "/";
            }
        }
        holder.date.setText(tempDate);

        holder.description=(TextView) rowView.findViewById(R.id.card_description);
        holder.description.setText(entries.get(position).getDescription());

        holder.checkNo=(TextView) rowView.findViewById(R.id.card_check);
        if(entries.get(position).getCheckNo() == -1) {
            holder.checkNo.setText("");
        }
        else {

            holder.checkNo.setText(Integer.toString(entries.get(position).getCheckNo()));
        }

        holder.credit=(TextView) rowView.findViewById(R.id.card_credit);
        if(entries.get(position).getCredit() != null) {
            String credit = String.format("%.2f", entries.get(position).getCredit());
            holder.credit.setText(credit);
        }
        else {
            holder.credit.setText("");
        }

        holder.debit=(TextView) rowView.findViewById(R.id.card_debit);
        if(entries.get(position).getDebit() != null) {
            String debit = String.format("%.2f", entries.get(position).getDebit());
            holder.debit.setText(debit);
        }
        else {
            holder.debit.setText("");
        }


        holder.balance=(TextView) rowView.findViewById(R.id.card_balance);
        String balance = String.format("%.2f", entries.get(position).getBalance());
        holder.balance.setText(balance);




        rowView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return false;

            }
        });

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        return rowView;
    }


}
