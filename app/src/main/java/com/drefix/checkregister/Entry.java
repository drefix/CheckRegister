package com.drefix.checkregister;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by drefix on 9/21/17.
 */

public class Entry implements Serializable {
    int date[];
    String description;
    int checkNo;
    Double credit;
    Double debit;
    Double balance;

    public int[] getDate(){ return date;}
    public String getDescription(){return description;}
    public int getCheckNo(){return checkNo;}
    public Double getCredit(){return credit;}
    public Double getDebit(){return debit;}
    public Double getBalance(){return balance;}


}
